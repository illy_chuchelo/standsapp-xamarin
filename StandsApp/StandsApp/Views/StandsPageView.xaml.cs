﻿using Prism.Mvvm;
using StandsApp.ViewModels;
using StandsApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace StandsApp.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class StandsPageView : BaseContentPage
    {
        public StandsPageView()
        {
            InitializeComponent();
        }
    }
}
