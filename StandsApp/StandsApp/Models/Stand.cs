﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandsApp.Models
{
    public class Stand
    {
        public string StandUser { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Season { get; set; }
        public string Img { get; set; }
        public string ImgUser { get; set; }

    }
}
