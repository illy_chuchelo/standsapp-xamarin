﻿using StandsApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StandsApp.Src
{
    public static class StandsData
    {
        public static List<Stand> GetStands()
        {
            return new List<Stand>()
            {
                new Stand { StandUser = "Dio", 
                    Name = "The World", 
                    Description = "Стенд Дио Брандо, показанный в «Крестоносцах звёздной пыли».", 
                    Season = 2,
                    Img = "https://vignette.wikia.nocookie.net/jojo/images/4/41/TheWorld_AnimeAV.png/revision/latest?cb=20160705123415&path-prefix=ru",
                    ImgUser = "https://vignette.wikia.nocookie.net/jojo/images/0/0a/DIO_Normal_SC_Infobox_Anime.png/revision/latest?cb=20200121135429&path-prefix=ru"},
                new Stand { StandUser = "Jotaro", 
                    Name = "Star Platinum", 
                    Description ="Среди самых первых представленных стендов, он появлялся вместе с Джотаро в трех частях аниме и манги, первая из которых была о Крестоносцах Звездной пыли.", 
                    Season = 2,
                    Img="https://vignette.wikia.nocookie.net/jojo/images/c/ca/Star_Platinum_SC_Infobox_Anime.png/revision/latest?cb=20200219085657&path-prefix=ru",
                    ImgUser = "https://vignette.wikia.nocookie.net/jjba/images/0/01/JotaroProfile.png/revision/latest?cb=20191125014406"},
                new Stand { StandUser = "Polnareff", 
                    Name = "Silver Chariot", 
                    Description ="Один из дейтерагонистов «Крестоносцев звёздной пыли» и второстепенный персонаж в «Золотом ветре».", 
                    Season = 2,
                    Img="https://vignette.wikia.nocookie.net/jjba/images/3/3c/SilverChariotPart5Anime.png/revision/latest?cb=20190622182239",
                    ImgUser = "https://vignette.wikia.nocookie.net/jojo/images/4/49/341.png/revision/latest?cb=20180228135139&path-prefix=ru"},
                

                new Stand { StandUser = "Josuke", 
                    Name = "Crazy Diamond", 
                    Description ="Стенд Джоске Хигашикаты, показанный в «Несокрушимом алмазе».", 
                    Season = 3, 
                    Img ="https://vignette.wikia.nocookie.net/jjba/images/c/c5/Crazy_Diamond_Anime.png/revision/latest?cb=20160414081459", 
                    ImgUser = "https://vignette.wikia.nocookie.net/jojo/images/4/49/Josuke_DU_Infobox_Anime.png/revision/latest?cb=20200121132911&path-prefix=ru" },
                new Stand { StandUser = "Rohan", 
                    Name = "Heaven's Door", 
                    Description ="Изначально появляется как способность без внешнего вида, но приобретает полноценное тело в последующих арках.", 
                    Season = 3,
                    Img ="https://vignette.wikia.nocookie.net/jojo/images/d/de/Heaven%27s_Door_AV.png/revision/latest?cb=20180404164148&path-prefix=ru",
                    ImgUser = "https://vignette.wikia.nocookie.net/jjba/images/4/4f/Rohan_anime_AV.png/revision/latest?cb=20171212082327"},
                new Stand { StandUser = "Kira", 
                    Name = "Killer Queen ", 
                    Description ="Мускулистый стенд примерно такого же роста, как и сам Кира, в целом светлый.", 
                    Season = 3, 
                    Img = "https://vignette.wikia.nocookie.net/jjba/images/2/21/KQ_prepares_a_bubble_bomb.png/revision/latest?cb=20161210045842",
                    ImgUser = "https://vignette.wikia.nocookie.net/jjba/images/9/94/KiraBDset.jpg/revision/latest?cb=20181220053841" },

                new Stand { StandUser = "Gio", 
                    Name = "Gold Experience", 
                    Description ="Гуманоидный стенд стройного телосложения и среднего роста, как у Джорно.", 
                    Season = 4, 
                    Img = "https://vignette.wikia.nocookie.net/jjba/images/8/81/Gold_Experience_Anime.png/revision/latest?cb=20190106204524",
                    ImgUser="https://vignette.wikia.nocookie.net/jojo/images/1/1e/GiornoAV.png/revision/latest?cb=20200121134524&path-prefix=ru"},
                new Stand { StandUser = "Bucharati", 
                    Name = "Sticky Fingers", 
                    Description ="Верхняя половина лица стенда скрыта шлемом с короткими шипами, образующими ирокез.", 
                    Season = 4, 
                    Img = "https://vignette.wikia.nocookie.net/jjba/images/1/1d/Sticky_fingers_anime.png/revision/latest?cb=20181205014317",
                    ImgUser = "https://vignette.wikia.nocookie.net/jjba/images/9/9d/Bucciarati_Bluray.png/revision/latest?cb=20191006225749"},
                new Stand { StandUser = "Trish", 
                    Name = "Spice Girl", 
                    Description ="На её теле можно заметить различные математические знаки: плюсы на обоих плечах и локтях, а также на лбу, знак деления в области бровей и умножения - на макушке.", 
                    Season = 4, 
                    Img = "https://vignette.wikia.nocookie.net/jjba/images/6/6d/Spice_girl_anime_infobox.png/revision/latest?cb=20190405200617",
                    ImgUser ="https://vignette.wikia.nocookie.net/jjba/images/f/f6/Trish_Una_-_Anime.png/revision/latest?cb=20190617021603"},

            };
        }
    }
}
