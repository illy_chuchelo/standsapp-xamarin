﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace StandsApp.Controls
{
    public class ExtendedListView : ListView
    {
        public ExtendedListView()
        {
            this.ItemTapped += (sender, e) =>
            {
                if (TappedCommand != null)
                {
                    TappedCommand?.Execute(e.Item);
                }
                if (!CanSelect)
                {
                    SelectedItem = null;
                }
            };
        }

        #region -- Public properties --

        public static readonly BindableProperty TappedCommandProperty =
            BindableProperty.Create(nameof(TappedCommand), typeof(ICommand), typeof(ExtendedListView), default(ICommand));

        public ICommand TappedCommand
        {
            get => (ICommand)GetValue(TappedCommandProperty);
            set => SetValue(TappedCommandProperty, value);
        }

        public static readonly BindableProperty CanSelectProperty =
            BindableProperty.Create(nameof(CanSelect), typeof(bool), typeof(ExtendedListView), false);

        public bool CanSelect
        {
            get => (bool)GetValue(CanSelectProperty);
            set => SetValue(CanSelectProperty, value);
        }

        public static readonly BindableProperty SpacingProperty =
            BindableProperty.Create(nameof(Spacing), typeof(int), typeof(ExtendedListView), 0);

        public int Spacing
        {
            get => (int)GetValue(SpacingProperty);
            set => SetValue(SpacingProperty, value);
        }

        #endregion

    }
}
