﻿using Prism.Ioc;
using Prism.Navigation.Xaml;
using Prism.Unity;
using StandsApp.ViewModels;
using StandsApp.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StandsApp
{
    public partial class App : PrismApplication
    {

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //containerRegistry.RegisterInstance<>

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<StandsPageView>();
            containerRegistry.RegisterForNavigation<DetailsPageView>();
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            MainPage = new NavigationPage();
            await NavigationService.NavigateAsync(nameof(StandsPageView));
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
