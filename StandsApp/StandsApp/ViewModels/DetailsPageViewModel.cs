﻿using Prism.Navigation;
using StandsApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StandsApp.ViewModels
{
    public class DetailsPageViewModel : BaseViewModel
    {
        public DetailsPageViewModel()
        {
            
        }

        private Stand stand;
        public Stand Stand
        {
            get => stand;
            set => SetProperty(ref stand, value);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("StandInfo"))
            {
                Stand = parameters.GetValue<Stand>("StandInfo");
            }
        }
    }
}
