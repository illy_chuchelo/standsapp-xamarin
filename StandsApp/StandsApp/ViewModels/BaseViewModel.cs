﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Prism.Mvvm;
using Prism.Navigation;

namespace StandsApp.ViewModels
{
    public class BaseViewModel : BindableBase, INavigationAware
    {

        public BaseViewModel()
        {
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
            
        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
            
        }
    }
}
