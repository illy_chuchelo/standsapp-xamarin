﻿using Prism.Navigation;
using StandsApp.Models;
using StandsApp.Src;
using StandsApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace StandsApp.ViewModels
{
    public class StandsPageViewModel : BaseViewModel
    {
        private INavigationService navigationService;

        public StandsPageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;

            Stands = StandsData.GetStands();
        }

        private List<Stand> stands;

        public List<Stand> Stands
        {
            get => stands;
            set => SetProperty(ref stands, value);
        }

        public ICommand GoToDetailsCommand => new Command(GoToDetails);

        private async void GoToDetails(object obj)
        {
            if (obj is Stand stand)
            {
                NavigationParameters parameters = new NavigationParameters
                {
                    { "StandInfo", stand },
                };
                await navigationService.NavigateAsync("DetailsPageView", parameters);
            }
        }
    }
}
